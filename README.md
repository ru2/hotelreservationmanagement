# Hotel Reservation Managment
This application source code can only be used for educational purposes. 

## Table of Contents

1. [ Description ](#desc)
2. [ General Install ](#setup)

## 1. Description
This project was inspired by learning Spring Boot (Frank Moley). The main purpose of creating this application was to check hotel reservations using different criteria. There were added some extra functionalities such as find reservations by name, last name or first name. There were also added integrations tests and templates.

Stack used in this project was Spring Boot Framework, Hibernate, Maven, Rest, JUnit, Mockito, JPA, Thymeleaf, H2 database, Apache Tomcat, HTML, CSS, JSON, Docker.

## 2. General Setup
Project requires Apache Maven to be installed on machine.
* The project requires Apache Maven to be installed on the machine. Please use start_postgres.sh from the bin folder to start importing demo data. After the script finishes its process date - start application.
* Connect your application using 
 [http://localhost:8080/reservations/](http://localhost:8080/reservations/)

Following api specifications are available (by example):

* Find reservation by date - [http://localhost:8080/reservations/2020-01-01](http://localhost:8080/reservations/2020-01-01)

* Find all guests - [http://localhost:8080/guests/](http://localhost:8080/guests/)

* Find reservations by guest Name or Surname - [http://localhost:8080/guests?name=Young](http://localhost:8080/guests?name=Young)