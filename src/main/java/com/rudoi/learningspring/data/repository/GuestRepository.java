package com.rudoi.learningspring.data.repository;

import com.rudoi.learningspring.data.entity.Guest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GuestRepository extends CrudRepository<Guest, Long> {
    List<Guest> findByLastNameOrFirstName(String lastName, String firstName);
}
