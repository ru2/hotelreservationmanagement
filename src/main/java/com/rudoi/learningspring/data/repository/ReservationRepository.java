package com.rudoi.learningspring.data.repository;

import com.rudoi.learningspring.data.entity.Reservation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {
    List<Reservation> findReservationByReservationDate(Date date);
    List<Reservation> findReservationByGuestId(Long guestId);
}
