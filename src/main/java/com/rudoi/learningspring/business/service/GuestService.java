package com.rudoi.learningspring.business.service;

import com.rudoi.learningspring.business.domain.GuestReservation;
import com.rudoi.learningspring.data.entity.Guest;
import com.rudoi.learningspring.data.entity.Reservation;
import com.rudoi.learningspring.data.entity.Room;
import com.rudoi.learningspring.data.repository.GuestRepository;
import com.rudoi.learningspring.data.repository.ReservationRepository;
import com.rudoi.learningspring.data.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GuestService {
    private final RoomRepository roomRepository;
    private final GuestRepository guestRepository;
    private final ReservationRepository reservationRepository;

    @Autowired
    public GuestService(RoomRepository roomRepository, GuestRepository guestRepository, ReservationRepository reservationRepository) {
        this.roomRepository = roomRepository;
        this.guestRepository = guestRepository;
        this.reservationRepository = reservationRepository;
    }

    public List<GuestReservation> getReservationByFirstNameOrLastName(String lastName, String firstName) {

        List<Guest> guestsMatchNameOrSurname = this.guestRepository.findByLastNameOrFirstName(lastName, firstName);
        Map<Long, GuestReservation> guestReservationMap = new HashMap<>();

        for(Guest guest : guestsMatchNameOrSurname) {
            Long guestId = guest.getGuestId();
            if (Objects.nonNull(this.reservationRepository.findReservationByGuestId(guestId))) {
                List<Reservation> reservations = this.reservationRepository
                        .findReservationByGuestId(guestId);

                reservations.forEach(
                        reservation -> {
                            GuestReservation guestReservation = new GuestReservation();
                            Room room = this.roomRepository.findById(reservation.getRoomId()).get();
                            guestReservation.setRoomId(reservation.getRoomId());
                            guestReservation.setRoomName(room.getRoomName());
                            guestReservation.setRoomNumber(room.getRoomNumber());
                            guestReservation.setFirstName(guest.getFirstName());
                            guestReservation.setLastName(guest.getLastName());
                            guestReservation.setGuestId(guest.getGuestId());
                            guestReservation.setDate(reservation.getReservationDate());
                            guestReservationMap.put(reservation.getReservationId(), guestReservation);
                        }
                );
            }
        }

        List<GuestReservation> guestReservations = new ArrayList<>();
        for (Long id : guestReservationMap.keySet()) {
            guestReservations.add(guestReservationMap.get(id));
        }
        return guestReservations;
    }
}
