package com.rudoi.learningspring.web;

import java.sql.Date;
import java.time.LocalDate;

public class DateUtils {
    public static Date convertFromStringData(String date) {
        Date newDate = null;
        if (date != null) {
            try {
                newDate = Date.valueOf(date);
            } catch (Exception e) {
                newDate = Date.valueOf(LocalDate.now());
            }
        } else {
            newDate = Date.valueOf(LocalDate.now());

        }
        return newDate;
    }
}