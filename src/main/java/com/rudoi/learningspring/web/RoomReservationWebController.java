package com.rudoi.learningspring.web;

import com.rudoi.learningspring.business.domain.RoomReservation;
import com.rudoi.learningspring.business.service.ReservationService;
import com.rudoi.learningspring.data.entity.Reservation;
import com.rudoi.learningspring.data.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/reservations")
public class RoomReservationWebController {
    private final ReservationService reservationService;
    private final ReservationRepository reservationRepository;

    @Autowired
    public RoomReservationWebController(ReservationService reservationService, ReservationRepository reservationRepository) {
        this.reservationService = reservationService;
        this.reservationRepository = reservationRepository;
    }

    @GetMapping
    public String getReservations(Model model) {
        Iterable<Reservation> reservations = this.reservationRepository.findAll();
 /*       List<RoomReservation> roomReservations = new ArrayList<>();
        reservations.forEach(
                reservation -> {
                    RoomReservation roomReservation = new RoomReservation();
                    roomReservation.setDate(reservation.getReservationDate());
                    roomReservation.setRoomId(reservation.getRoomId());
                    roomReservation.setGuestId(reservation.getGuestId());
                    roomReservations.add(roomReservation);
                }
        );*/
        model.addAttribute("reservation", reservations);
        return "reservation";
    }

    @GetMapping("/{stringDate}")
    public String getRoomReservationsByDate(@PathVariable String stringDate, Model model) {
        Date date = DateUtils.convertFromStringData(stringDate);
        List<RoomReservation> roomReservations = this.reservationService.getRoomReservationsForDate(date);
        model.addAttribute("reservationsByDate", roomReservations);
        // reservations - variable name that will be used in html template
        // roomReservations - object that you pass from java to html
        // reservation - name of html template - reservation.html
        return "reservationbydate";
    }
}

