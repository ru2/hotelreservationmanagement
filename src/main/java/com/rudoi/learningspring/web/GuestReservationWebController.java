package com.rudoi.learningspring.web;

import com.rudoi.learningspring.business.domain.GuestReservation;
import com.rudoi.learningspring.business.domain.RoomReservation;
import com.rudoi.learningspring.business.service.GuestService;
import com.rudoi.learningspring.data.entity.Guest;
import com.rudoi.learningspring.data.repository.GuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping()
public class GuestReservationWebController {
    private GuestService guestService;
    private GuestRepository guestRepository;

    @Autowired
    public GuestReservationWebController(GuestService guestService, GuestRepository guestRepository ){
        this.guestRepository = guestRepository;
        this.guestService = guestService;
    }

    @GetMapping("/guests")
    public String getAllGuests(Model model) {
        Iterable<Guest> allGuests = this.guestRepository.findAll();
        model.addAttribute("guests", allGuests);
        return "guests";
    }

    @RequestMapping(value = "/guests", params = {"lastname", "firstname"}, method = RequestMethod.GET)
    public String getResevationByFirstNameOrLastName(@RequestParam("lastname") String lastName,  @RequestParam("firstname") String firstName, Model model){
        List<GuestReservation> allMatchFirstNameOrLastName = this.guestService.getReservationByFirstNameOrLastName(lastName, firstName);
        model.addAttribute("guests", allMatchFirstNameOrLastName);
        return "reservationByGuestName";
    }

    @RequestMapping(value = "/guests", params = {"name"}, method = RequestMethod.GET)
    public String getResevationByName(@RequestParam("name") String name, Model model){
        List<GuestReservation> allMatchFirstNameOrLastName = this.guestService.getReservationByFirstNameOrLastName(name, name);
        model.addAttribute("guests", allMatchFirstNameOrLastName);
        return "reservationByGuestName";
    }
}
