package com.rudoi.learningspring;

import com.rudoi.learningspring.business.domain.RoomReservation;
import com.rudoi.learningspring.business.service.ReservationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.sql.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LearningSpringApplicationTests {
    @Autowired
    private ReservationService reservationService;

    @Test
    public void contextLoads() {
    }

    @GetMapping("/{date}")
    public List<RoomReservation> getRoomReservationsByDate(@PathVariable Date date) {
//        LocalDate date1 = LocalDate.parse(date);
        return reservationService.getRoomReservationsForDate(date);
    }
}
