package com.rudoi.learningspring.integrationtests;

import com.rudoi.learningspring.business.domain.RoomReservation;
import com.rudoi.learningspring.business.service.ReservationService;
import com.rudoi.learningspring.data.entity.Reservation;
import com.rudoi.learningspring.data.repository.ReservationRepository;
import com.rudoi.learningspring.web.DateUtils;
import com.rudoi.learningspring.web.RoomReservationWebController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(RoomReservationWebController.class)
public class RoomReservationWebControllerTest {
    @MockBean
    private ReservationService reservationService; // for immitating my object

    @MockBean
    private ReservationRepository reservationRepository;

    @Autowired
    private MockMvc mockMvc;                // for immitating server side behavior

    @Test
    public void getRoomReservationsFromControllerTest() throws Exception{
        Date date = DateUtils.convertFromStringData("2020-01-01");
        List<RoomReservation> roomReservations = new ArrayList<>();
        RoomReservation roomReservation = new RoomReservation();
        roomReservation.setDate(date);
        roomReservation.setGuestId(1);
        roomReservation.setLastName("JUnit");
        roomReservation.setFirstName("Unit");
        roomReservation.setRoomId(7);
        roomReservation.setRoomName("Cambridge");
        roomReservation.setRoomNumber("C1");

        roomReservations.add(roomReservation);

        given(reservationService.getRoomReservationsForDate(date)).willReturn(roomReservations);

        this.mockMvc.perform(
            get("/reservations/2020-01-01"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("JUnit, Unit")));
    }

    @Test
    public void getRoomReservationsTest() throws Exception{
        Date date = DateUtils.convertFromStringData("2020-01-01");
        List<Reservation> roomReservations = new ArrayList<>();
        Reservation reservation = new Reservation();
        reservation.setReservationDate(date);
        reservation.setGuestId(200);
        reservation.setRoomId(8);

        roomReservations.add(reservation);

        given(reservationRepository.findAll()).willReturn(roomReservations);

        this.mockMvc.perform(
                get("/reservations"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("200")));
    }
}
