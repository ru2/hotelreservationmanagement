package com.rudoi.learningspring.integrationtests;

import com.rudoi.learningspring.business.domain.GuestReservation;
import com.rudoi.learningspring.business.domain.RoomReservation;
import com.rudoi.learningspring.business.service.GuestService;
import com.rudoi.learningspring.data.entity.Guest;
import com.rudoi.learningspring.data.repository.GuestRepository;
import com.rudoi.learningspring.web.GuestReservationWebController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(GuestReservationWebController.class)
public class GuestReservationWebControllerTest {
    @MockBean
    private GuestService guestService;

    @MockBean
    private GuestRepository guestRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void checkReservationByGuestNameTest() throws Exception{

        List<GuestReservation> guestReservations = new ArrayList<>();
        GuestReservation reservation = new GuestReservation();
        reservation.setGuestId(1);
        reservation.setLastName("JUnit");
        reservation.setFirstName("Unit");

        guestReservations.add(reservation);

        given(guestService.getReservationByFirstNameOrLastName("JUnit", "Unit")).willReturn(guestReservations);

        this.mockMvc.perform(
                get("/guests?lastname=JUnit&firstname=Unit"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Unit")));

    }
}
